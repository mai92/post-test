<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @if(session('success'))
                <div class="p-2 my-4 bg-green-500 border-b border-gray-200 text-white">
                    {{ session('success') }}
                </div>
            @endif

            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form action="{{ route('student.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="mb-2">
                            <x-label>Name</x-label>
                            <x-input name="name" type="text" class="w-full"></x-input>
                        </div>

                        <div class="mb-2">
                            <x-label>Email</x-label>
                            <x-input name="email" type="text" class="w-full"></x-input>
                        </div>

                        <div class="mb-2">
                            <x-label>Address</x-label>
                            <x-input name="address" type="text" class="w-full"></x-input>
                        </div>

                        <div class="mb-2">
                            <x-label>Phone Number</x-label>
                            <x-input name="phone_number" type="text" class="w-full"></x-input>
                        </div>

                        <div class="mb-2">
                            <x-label>Photo</x-label>
                            <x-input name="photo" type="file" class="w-full"></x-input>
                        </div>

                        <div class="mb-2 flex justify-end">
                            <x-button type="submit">Save</x-button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</x-app-layout>
